from flask import Flask, request, render_template
from models.decoder import apply_model_to_image_raw_bytes
import models.download_utils as download_utils
import os

app = Flask(__name__)

@app.route('/')
def index():
    return app.send_static_file('index.html')

@app.route('/test')
def test():
    link = request.args.get('link')
    sentence = ''
    if link:
        download_utils.download_file(link, "static/test.jpg")
        sentence = apply_model_to_image_raw_bytes(open("static/test.jpg", "rb").read())
    return str(sentence)

if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)

